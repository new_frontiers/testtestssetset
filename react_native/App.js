
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar
} from 'react-native';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
/*
import reducer from './app/Redux/reducers';
import { setNavigator, setActiveRoute } from "./app/Redux/actions";
let store = createStore(reducer);
*/

import reducer from './reducer';

import MenuItem from './menuElement.js';
import CampaignInfo from './campaignInfo.js';

const serverHost = "192.168.43.207";
const serverPort = 5070;

console.disableYellowBox = true; // Disable warnings on a phone screen

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaigns: false,
            error: false
        }
    }

    async componentDidMount() {

        this.getCampaign(100000003);
    }

    async getCampaigns() {
        const response = await fetch('http://' + serverHost + ':' + serverPort + '/api/v1/campaigns');
        const body = await response.json();

        if (response.status !== 200) {
          return this.setState({
              error: body.message
          })
        }

        this.setState({
            campaigns: body.campaigns
        })
    }

    async getCampaign(id) {

        console.log("URL:", 'http://' + serverHost + ':' + serverPort + '/api/v1/campaign/' + id)

        const response = await fetch('http://' + serverHost + ':' + serverPort + '/api/v1/campaign/' + id);
        const body = await response.json();

        if (response.status !== 200) {
            return this.setState({
                error: body.message
            })
        }

        this.setState({
            campaign: body.campaign
        })

    }

    render() {

        console.log("********* render $$$$$  11111 *(*******************");
        console.log(this.state)

        if (!this.state.campaign) return (<Text>Loading...</Text>);

        return (<View>

            <CampaignInfo data={this.state.campaign}/></View>)

      /*
        return (
            <Provider store={store}>

            </Provider>
        );
*/
      /*
        return (<View>

                  {this.state.campaigns && this.state.campaigns.map((value, index) => {
                      return <MenuItem value={value.name}/>
                  })}

              </View>)

              */
    };
};

export default App;
