import React from 'react';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

const PlatformGeneral = ({data}) => {

    return (
        <View style={styles.stylesWrapper}>
            <Text>{data.status}</Text>
            <Text>{data.total_budget}</Text>
            <Text>{data.remaining_budget}</Text>
            <Text>{data.start_date}</Text>
            <Text>{data.end_date}</Text>
            <Text>{data.remaining_budget}</Text>
            <Text>{data.remaining_budget}</Text>
        </View>

    );
}
const styles = StyleSheet.create({
    stylesWrapper: {
        marginTop: 20
    },
    label: {
        width: "40%",
        height: 50,
        backgroundColor: "#F28627",
        textAlign: "right",
        fontSize: 34
    },
    data: {
        fontSize: 34,
        height: 50,
    },
    goal: {
        fontSize: 24,
    },
    element: {
        flexWrap:'wrap',
        flexDirection:'row',
    }
});

export default PlatformGeneral;