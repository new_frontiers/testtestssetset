import React from 'react';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import PlatformStat from './platformStat.js';

const CampaignInfo = ({data}) => {
    
    return (
        <View style={styles.stylesWrapper}>
            <View style={styles.element}>
                <Text style={styles.label}>Name: </Text>
                <Text style={styles.data}>{data.name}</Text>
            </View>

            <View style={styles.element}>
                <Text style={styles.label}>Goal: </Text>
                <Text style={[styles.name, styles.goal]}>{data.goal}</Text>
            </View>

            <View style={styles.element}>
                <Text style={styles.label}>Budget: </Text>
                <Text style={styles.data}>{data.total_budget}</Text>
            </View>

            <View style={styles.element}>
                <Text style={styles.label}>Status: </Text>
                <Text style={styles.data}>{data.status}</Text>
            </View>

            <PlatformStat data={data.platforms.facebook} visible={true}/>
            <PlatformStat data={data.platforms.instagram} visible={false}/>

        </View>

    );
}

const styles = StyleSheet.create({
    stylesWrapper: {
        marginTop: 20
    },
    label: {
        width: "40%",
        height: 50,
        backgroundColor: "#F28627",
        textAlign: "right",
        fontSize: 34
    },
    data: {
        fontSize: 34,
        height: 50,
    },
    goal: {
        fontSize: 24,
    },
    element: {
        flexWrap:'wrap',
        flexDirection:'row',
    }
});

export default CampaignInfo;