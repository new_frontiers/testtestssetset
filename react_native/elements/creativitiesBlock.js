import React from 'react';

import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';

const CreativitiesBlock = ({data, visible}) => {

    return (
            <View style={styles.stylesWrapper}>

                {(visible) ? (
                    <View>
                        <Text>{data.header}</Text>
                        <Text>{data.description}</Text>
                        <Text>{data.url}</Text>
                        <Text>{data.image}</Text>
                    </View>
                ) : (<View></View>)}
            </View>
        );
}


//  <Image
//                     style={styles.productImg}
//                     source={require('/Users/turbo/nanos/fullstack-dev-assessment/images/img1.jpg')}/>
// <Image source={require('../images/test.png')}  style={{width: 400, height: 400}} />

const styles = StyleSheet.create({
    stylesWrapper: {
        marginTop: 20
    },
    label: {
        width: "40%",
        height: 50,
        backgroundColor: "#F28627",
        textAlign: "right",
        fontSize: 34
    },
    data: {
        fontSize: 34,
        height: 50,
    },
    goal: {
        fontSize: 24,
    },
    element: {
        flexWrap:'wrap',
        flexDirection:'row',
    }
});

export default CreativitiesBlock;