import React from 'react';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import CreativitiesBlock from './creativitiesBlock.js';
import AudianceBlock from './audianceBlock.js';
import PlatformGeneral from './platformGeneral.js';

const PlatformStat = ({data, visible}) => {

    return (
        <View style={styles.stylesWrapper}>

            {(visible) ? (

                <View>
                    <PlatformGeneral data={data} visible={true}/>

                    <CreativitiesBlock data={data.creatives} visible={false}/>
                    <AudianceBlock data={data.target_audiance} visible={false}/>
                </View>

            ) : (<View></View>)}

        </View>

    );
}
const styles = StyleSheet.create({
    stylesWrapper: {
        marginTop: 20
    },
    label: {
        width: "40%",
        height: 50,
        backgroundColor: "#F28627",
        textAlign: "right",
        fontSize: 34
    },
    data: {
        fontSize: 34,
        height: 50,
    },
    goal: {
        fontSize: 24,
    },
    element: {
        flexWrap:'wrap',
        flexDirection:'row',
    }
});

export default PlatformStat;