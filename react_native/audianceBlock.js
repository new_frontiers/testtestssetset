import React from 'react';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

const AudianceBlock = ({data, visible}) => {

    return (
        <View style={styles.stylesWrapper}>

            {(visible) ? (

                <View>
                    <View>
                        <Text>Languages:</Text>
                        {data.languages && data.languages.map((value, index) => {
                            return <Text>{value}</Text>
                        })}
                    </View>

                    <View>
                        <Text>Genders:</Text>
                        {data.genders && data.genders.map((value, index) => {
                            return <Text>{value}</Text>
                        })}
                    </View>
                </View>

            ) : (<View></View>)}

        </View>
        );
}

const styles = StyleSheet.create({
    stylesWrapper: {
        marginTop: 20
    },
    label: {
        width: "40%",
        height: 50,
        backgroundColor: "#F28627",
        textAlign: "right",
        fontSize: 34
    },
    data: {
        fontSize: 34,
        height: 50,
    },
    goal: {
        fontSize: 24,
    },
    element: {
        flexWrap:'wrap',
        flexDirection:'row',
    }
});

export default AudianceBlock;