import React from 'react';

import {
    Text,
    StyleSheet
} from 'react-native';

const MenuItem = ({value}) => {
    return (
        <Text style={styles.element}></Text>
    );
}

const styles = StyleSheet.create({
    element: {
        width: "100%",
        height: 40,
        backgroundColor: "#F28627"
    }
});

export default MenuItem;